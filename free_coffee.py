import datetime as dt

import pandas as pd
import requests
from bs4 import BeautifulSoup as bs


def free_coffee():
    site_response = requests.get('https://www.hockey-reference.com/teams/NSH/2018_games.html')
    bs_page = bs(site_response.text, 'lxml')

    # table of schedule with results
    div_schedule = bs_page.find('div', {'id': 'all_games'})

    df = pd.read_html(str(div_schedule))[0]
    df.columns = df.columns.droplevel()
    df.columns = [i.lower().replace(' ', '_') for i in df.columns]

    today_ = dt.datetime.now()
    yesterday_ = today_ - dt.timedelta(days=1)
    date_string = yesterday_.strftime('%Y-%m-%d')

    if date_string not in df['date'].tolist():
        return 'Did not play yesterday: {}'.format(date_string)

    yesterdays_event = df.loc[df['date'] == date_string]
    game_result = yesterdays_event['unnamed:_7_level_1'].str.lower().tolist()[0]
    if game_result == 'w':
        return 'Get coffee, they won: {}, {}'.format(yesterdays_event['opponent'].tolist()[0], date_string)
    elif game_result == 'l':
        return 'No coffee, they lost: {} {}'.format(yesterdays_event['opponent'].tolist()[0], date_string)
    else:
        return 'Whoa.  There was an error.  Result was neither win nor loss.'


if __name__ == '__main__':
    print(free_coffee())
